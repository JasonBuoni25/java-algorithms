import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DynamicProgramming {

    static int longestCommonSubstring(String a, String b) {
        List<Integer[]> t = new ArrayList<Integer[]>();

        int max_i = 0, max_j = 0;
        //Init
        for(int i = 0; i < a.length(); i++) {
            Integer[] init = new Integer[b.length()];
            Arrays.fill(init, 0);
            t.add(init);
        }

        for(int i = 0; i < a.length(); i++) {
            for(int j = i; j < b.length(); j++) {
                if(i == 0 || j == 0 || a.charAt(i) != b.charAt(j)) {
                    t.get(i)[j] = 0;
                }
                else {
                    t.get(i)[j] = 1 + t.get(i - 1)[j - 1];
                }

                if(t.get(i)[j] > t.get(max_i)[max_j]) {
                    max_i = i;
                    max_j = j;
                }
            }
        }

        return t.get(max_i)[max_j];

    }

    static int longestPalindromeString(String s) {
        List<Integer[]> t = new ArrayList<Integer[]>();

        int max_i = 0, max_j = 0;
        int length = s.length();
        //Init
        for(int i = 0; i < length; i++) {
            Integer[] init = new Integer[length];
            Arrays.fill(init, 0);
            t.add(init);
        }

        for(int i = 0; i < length; i++) {
            t.get(i)[i] = 1;
        }
        for(int i = 0; i < length; i++) {
            for(int j = i; j < length; j++) {
                if(s.charAt(i) == s.charAt(j)) {
                    t.get(i)[j] = t.get(i)[j];
                }
            }
        }

        return 0;
    }

    static int fibonacciModified(int t1, int t2, int n) {
        BigInteger[] fib = new BigInteger[n];
        fib[0] = BigInteger.valueOf(t1);
        fib[1] = BigInteger.valueOf(t2);

        for(int i = 2; i < n; i++) {
            // This is why I like Python better...
            double pow = Math.pow(fib[i - 1].doubleValue(), 2);
            BigInteger powAsBI = BigDecimal.valueOf(pow).toBigInteger();
            BigInteger fibValue = fib[i - 2].add(powAsBI);
            // Just add now
            fib[i] = fibValue;
        }
        return fib[n - 1].intValue();

    }


    public static void main(String[] args) {
        String X = "OldSite:GeeksforGeeks.org";
        String Y = "NewSite:GeeksQuiz.com";

        System.out.println("Length of Longest Common Substring is " + longestCommonSubstring(X, Y));
        System.out.println(fibonacciModified(1, 10, 5));
    }
}
