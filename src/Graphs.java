import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Tree {

    int height(Node root, A currentHeight) {
        if (root == null) {
            return 0;
        }

        int leftHeight = height(root.left, currentHeight);
        int rightHeight = height(root.right, currentHeight);

       currentHeight.height = Math.max(currentHeight.height, 1 + leftHeight + rightHeight);

        return 1 + Math.max(leftHeight, rightHeight);
    }

    static class A {
        int height = Integer.MIN_VALUE;
    }

    static class Node {
        Node left, right;
        public int value;

        Node(int value) {
            this.value = value;
        }
    }
}

class DFS {
    static int furthestNode;
    static int maxValue = Integer.MIN_VALUE;
    static List<Integer>[] neighbors;

    /**
     * Do a DFS algorithm to get height of tree
     * @param node
     * @param neighbors
     */
    static void dfs(int node, List<Integer>[] neighbors) {
        boolean[] visited = new boolean[neighbors.length];
        Arrays.fill(visited, false);

        dfsUtil(node, 1, visited, neighbors);
    }

    static void dfsUtil(int node, int count, boolean[] visited, List<Integer>[] neighbors) {
        List<Integer> nNeighbors = neighbors[node];
        visited[node] = true;
        count++;

        for (int i : nNeighbors) {
            if (!visited[i]) {
                if (count >= maxValue) {
                    maxValue = count;
                    furthestNode = i;
                }
                dfsUtil(i, count, visited, neighbors);
            }
        }
    }

    static int getMaxValue() {
        return maxValue;
    }

    static int getHeight(List<Integer>[] neighbors) {
        // First do a random one
        dfs(1, neighbors);
        // Now do from furthest node
        dfs(furthestNode, neighbors);

        return maxValue;
    }
}

public class Graphs {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Tree.Node root = new Tree.Node(1);
        root.left = new Tree.Node(2);
        root.right = new Tree.Node(3);
        root.left.left = new Tree.Node(4);
        root.left.right = new Tree.Node(5);

        Tree.A a = new Tree.A();
        tree.height(root, a);

        System.out.println(a.height);

        int n = 5;
        DFS dfs = new DFS();
        DFS.neighbors = new List[n + 1];
        for(int i = 0; i < n+1 ; i++)
            DFS.neighbors[i] = new ArrayList<Integer>();

        /*create undirected edges */
        DFS.neighbors[1].add(2);
        DFS.neighbors[2].add(1);
        DFS.neighbors[1].add(3);
        DFS.neighbors[3].add(1);
        DFS.neighbors[2].add(4);
        DFS.neighbors[4].add(2);
        DFS.neighbors[2].add(5);
        DFS.neighbors[5].add(2);

        /* maxCount will have diameter of tree */
        System.out.println(DFS.getHeight(DFS.neighbors));
    }
}