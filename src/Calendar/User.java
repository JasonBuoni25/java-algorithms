package Calendar;
import Calendar.Calendar;

public class User {
    public int teamId, userId;
    public String name;
    Calendar calendar;

    public User(int userId, String name) {
        new User(userId, name, null);
    }

    public User(int userId, String name, Calendar calendar) {
        this.name = name;
        this.userId = userId;
        this.calendar = calendar;
    }

    public void setCalendar(Calendar calendar) { this.calendar = calendar; }

    public Calendar getCalendar() { return this.calendar; }

    public void setTeamId(int teamId) { this.teamId = teamId; }

    public void addEventToCalendar(Event event) {
        this.calendar.addEvent(event);
    }
}
