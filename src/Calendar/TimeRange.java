package Calendar;

class TimeRange {
    public int startHour, startMinutes;
    public TimeRange(int startHour, int startMinutes) {
        this.startHour = startHour;
        this.startMinutes = startMinutes;
    }
}