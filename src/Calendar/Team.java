package Calendar;
import Calendar.User;
import Calendar.Event;
import com.sun.scenario.animation.shared.TimerReceiver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Team {
    int teamId;
    ArrayList<User> users = new ArrayList<User>();

    public Team(int teamId) {
        this.teamId = teamId;
    }

    public void addUser(User user) {
        user.setTeamId(teamId);
        this.users.add(user);
    }

    public ArrayList<User> getUsers() { return this.users; }

    public User getUser(int userId) {
        for (User user: users) {
            if(user.userId == userId)
                return user;
        }

        return null;
    }

    public ArrayList<TimeRange> getAvailByDay(Day day, TimeRange startRange, TimeRange endRange, int duration) {
        ArrayList<TimeRange> ranges = getTimeRange(duration, startRange, endRange);
        ArrayList<TimeRange> availList = new ArrayList<TimeRange>();

        for (TimeRange range: ranges) {
            int rangeInt = range.startHour * 100 + range.startMinutes;
            boolean isAvail = true;
            for (User user: users) {
                ArrayList<Event> events = user.getCalendar().getEventsByDay(day);
                for(int x = 1; x < events.size(); x++) {
                    Event prevEvent = events.get(x - 1);
                    Event cEvent = events.get(x);

                    int preStart = prevEvent.getStartTime();
                    int preEnd = prevEvent.getEndTime();
                    int cStart = cEvent.getStartTime();
                    int cEnd = cEvent.getEndTime();

                    TimeRange nextRange = getDuration(new TimeRange(range.startHour, range.startMinutes), duration);
                    int nextT = nextRange.startHour * 100 + nextRange.startMinutes;

                    if(preStart <= rangeInt && rangeInt < preEnd) {
                      isAvail = false;
                      break;
                    } else if(nextT > cStart && nextT <= cEnd) {
                        isAvail = false;
                        break;
                    }
                }
            }

            if(isAvail)
                availList.add(new TimeRange(range.startHour, range.startMinutes));
        }

        return availList;
    }

    public HashMap<Day, ArrayList<TimeRange>> getAvailByWeek(TimeRange startRange, TimeRange endRange, int duration) {
        HashMap<Day, ArrayList<TimeRange>> avail = new HashMap<Day, ArrayList<TimeRange>>();
        for (Day day: Day.values()) {
            if(day != Day.SATURDAY && day != Day.SUNDAY) {
                ArrayList<TimeRange> availDay = getAvailByDay(day, startRange, endRange, duration);
                avail.put(day, availDay);
            }
        }

        return avail;
    }

    public ArrayList<TimeRange> getTimeRange(int duration, TimeRange startRange, TimeRange endRange) {

        ArrayList<TimeRange> ranges = new ArrayList<TimeRange>();
        TimeRange currentRange = new TimeRange(startRange.startHour, startRange.startMinutes);
        int current = startRange.startHour * 100 + startRange.startMinutes;
        int endInt = endRange.startHour * 100 + endRange.startMinutes;

        while(current < endInt) {
            currentRange = getDuration(currentRange, duration);
            current = currentRange.startHour * 100 + currentRange.startMinutes;
            ranges.add(new TimeRange(currentRange.startHour, currentRange.startMinutes));

        }

        return ranges;
    }

    public TimeRange getDuration(TimeRange range, int duration) {
        range.startMinutes += duration;
        while(range.startMinutes >= 60) {
            range.startHour++;
            range.startMinutes -= 60;
        }

        return new TimeRange(range.startHour, range.startMinutes);
    }

    private String getDayString(Day day) {
        String str = "";
        switch(day) {
            case MONDAY:
                str = "Monday";
                break;
            case TUESDAY:
                str = "Tuesday";
                break;
            case WEDNESDAY:
                str = "Wednesday";
                break;
            case THURSDAY:
                str = "Thursday";
                break;
            case FRIDAY:
                str = "Friday";
                break;
        }

        return str;
    }

    public static void main(String[] args) {
        // I don't have a lot of Java stuff set up on my new mac so I didn't write unit tests

        Team myTeam = new Team(1);

        User jason = new User(1, "Jason");
        User joe = new User(1, "Joe");


        Calendar jasonC = new Calendar();
        Calendar joeC = new Calendar();

        for (Day day: Day.values()) {
            if(day != Day.SATURDAY && day != Day.SUNDAY) {
                jasonC.addEvent(new Event(day, 9, 0, 9, 15));
                jasonC.addEvent(new Event(day, 10, 0, 11, 0));

                joeC.addEvent(new Event(day, 9, 30, 11, 0));
                joeC.addEvent(new Event(day, 11, 30, 13, 0));
                joeC.addEvent(new Event(day, 13, 30, 14, 0));
            }
        }

        TimeRange startRange = new TimeRange(9, 0);
        TimeRange endRange = new TimeRange(14, 0);

        jason.setCalendar(jasonC);
        joe.setCalendar(joeC);

        myTeam.addUser(jason);

        ArrayList<TimeRange> avail = myTeam.getAvailByDay(Day.MONDAY, startRange, endRange, 30);

        System.out.println("Available For Monday");
        for (TimeRange ar: avail) {
            String sMinutes = String.format("%02d", ar.startMinutes);
            System.out.println(ar.startHour + ":" + sMinutes);
        }

        myTeam.addUser(joe);


        avail = myTeam.getAvailByDay(Day.MONDAY, startRange, endRange, 30);

        System.out.println("Available For Monday with Joe");
        for (TimeRange ar: avail) {
            String sMinutes = String.format("%02d", ar.startMinutes);
            System.out.println(ar.startHour + ":" + sMinutes);
        }
        System.out.println("*************");
        System.out.println("Available For Week");

        HashMap<Day, ArrayList<TimeRange>> availWeek = myTeam.getAvailByWeek(startRange, endRange, 30);

//        for (HashMap.Entry<Day, ArrayList<TimeRange>> entry : availWeek.entrySet()) {
        for (Day day: Day.values()) {
            System.out.println( myTeam.getDayString(day));
            if(day != Day.SATURDAY && day != Day.SUNDAY) {
                for (TimeRange ar : availWeek.get(day)) {
                    String sMinutes = String.format("%02d", ar.startMinutes);
                    System.out.println(ar.startHour + ":" + sMinutes);
                }
            }
        }
    }
}
