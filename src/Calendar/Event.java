package Calendar;

import Calendar.Day;

public class Event {
    public int startHours, endHours, startMinutes, endMinutes;
    private Day day;

    public Event(Day day, int startHours, int startMinutes, int endHours, int endMinutes) {
        this.day = day;
        this.startHours = startHours;
        this.endHours = endHours;
        this.startMinutes = startMinutes;
        this.endMinutes = endMinutes;
    }

    public Day getDay() { return day; }

    public int getStartTime() {
        return startHours * 100 + startMinutes;
    }

    public int getEndTime() {
        return endHours * 100 + endMinutes;
    }

    public int getDayAsInt() {
        int returnV = 0;
        switch (day) {
            case SUNDAY:
                returnV = 1;
                break;
            case MONDAY:
                returnV = 2;
                break;
            case TUESDAY:
                returnV = 3;
                break;
            case WEDNESDAY:
                returnV = 4;
                break;
            case THURSDAY:
                returnV = 5;
                break;
            case FRIDAY:
                returnV = 6;
                break;
            case SATURDAY:
                returnV = 7;
                break;
        }

        return returnV;
    }
}
