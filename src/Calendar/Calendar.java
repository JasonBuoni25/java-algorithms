package Calendar;

import java.util.*;
import Calendar.Day;
import Calendar.Event;

public class Calendar {

    HashMap<Day, ArrayList<Event>> eventsByDay = new HashMap<Day, ArrayList<Event>>();

    public Calendar() {
        for (Day day: Day.values()) {
            eventsByDay.put(day, new ArrayList<Event>());
        }
    }

    public void addEvent(Event event) {
        // Probably a better way to do this
        ArrayList<Event> events = eventsByDay.get(event.getDay());
        events.add(event);
        eventsByDay.put(event.getDay(), events);
    }

    public ArrayList<Event> getEventsByDay(Day day) {
        ArrayList<Event> events = eventsByDay.get(day);

        Collections.sort(events, new Comparator<Event>(){
            public int compare(Event e1, Event e2) {
                Integer e1Int = e1.getStartTime();

                return e1Int.compareTo(e1.getStartTime());
            }
        });

        return events;
    }
}