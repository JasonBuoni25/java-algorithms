import org.omg.PortableInterceptor.INACTIVE;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class StringsArrays {

    static Node head = null;

    static List<Integer> getReverseList(List<Integer> list, int k) {
        for(int x = list.size() -1; x >= 0; x--)
            push(list.get(x));

        Node newHead = getReverseList(head, k);

        List<Integer> retList = new LinkedList<Integer>();
        do {
            retList.add(newHead.value);
            newHead = newHead.next;
        } while(newHead != null);

        return retList;
    }

    static Node getReverseList(Node head, int k) {
        Stack<Node> nodeStack = new Stack<Node>();
        Node current = head;
        Node previous = null;

        while(current != null) {
            int count = 0;
            while(current != null && count < k) {
                nodeStack.push(current);
                current = current.next;
                count++;
            }

            while(!nodeStack.empty()) {
               if(previous == null) {
                   previous = nodeStack.peek();
                   head = previous;
                   nodeStack.pop();
               } else {
                   previous.next = nodeStack.peek();
                   previous = previous.next;
                   nodeStack.pop();
               }
            }
        }

        previous.next = null;
        return head;
    }

    static class Node {
        int value;
        Node next;
    }

    static void push(int value) {
        if (head == null) {
            head = new Node();
            head.value = value;
        } else {
            Node node = new Node();
            node.value = value;
            node.next = head;
            head = node;
        }
    }

    static String getLongestCommonPrefix(String[] strings) {
        String result = "";
        Arrays.sort(strings);

        String str1 = strings[0], str2 = strings[strings.length - 1];
        int n1 = str1.length(), n2 = str2.length();

        for(int i = 0, j = 0; i < n1 && j < n2; i++, j++) {
            if (str1.charAt(i) != str2.charAt(j))
                break;

            result += str1.charAt(i);
        }

        return result;
    }

    static int fib(int n) {
        int[] fib = new int[n + 2];
        fib[0] = 0;
        fib[1] = 1;

        for(int i = 2; i <= n; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }

        return fib[n];
    }


    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<Integer> rList = getReverseList(list, 3);
        System.out.println(rList.toString());

        String[] arr = {"geeksforgeeks", "geeks", "geek", "geezer"};
        System.out.println(getLongestCommonPrefix(arr));

        System.out.println(fib(9));
    }
}
